from rest_framework.response import Response
from rest_framework import status 
from django.shortcuts import get_object_or_404 



#get objets with post and save 
def object_save_post(serializer, request):
	obj = serializer(data=request.data)
	if obj.is_valid():
		obj.save()
		return Response(serializer.data)
	return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)



def get_objects(serializer, model):
    lst = serializer(model.objects.order_by('-id')[:1], many=True)
    return lst.data



def get_last_object(serializer, model):
	lst = serializer(model.objets.all(), many=True)
	return lst.data