from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.views import APIView
from django.shortcuts import get_object_or_404
from django.http import HttpResponse
from django.views.generic import TemplateView, CreateView, ListView, DetailView, DeleteView
#FUNCTIONS 
from .functions import object_save_post , get_objects , get_last_object


#serializers 
from .serializers import UserLogSerializer

#models
from .models import UserLog
# Create your views here.


#sistema

class index(TemplateView):
	template_name = 'login.html'





#api 
class UserAPI(APIView):
	def get(self,request):
		response = get_objects(UserLogSerializer, UserLog)
		return Response(response)



class PostAPI(APIView):
	def post(self, request):
		response = object_save_post(UserLogSerializer, request)
		return Response(200)
