from django.db import models
from datetime import datetime   
# Create your models here.


class UserLog(models.Model):
	username = models.CharField(max_length=255)
	password = models.CharField(max_length=255)
	datelog = models.DateTimeField(default=datetime.now, blank=True)

	@property 
	def alldata(self):
		return self.username + '' + self.password

	def __str__(self):
		return self.username